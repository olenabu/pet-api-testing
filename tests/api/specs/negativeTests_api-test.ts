import { checkResponseTime, checkStatusCode } from "../../helpers/functionsForChecking.helper";
import { AuthController } from "../lib/controllers/auth.controller";
import { UsersController } from "../lib/controllers/users.controller";
const auth = new AuthController();
const users = new UsersController();

describe("Use test data set for login", () => {
    let invalidCredentialsDataSet = [
        { email: "negativetest@gmail.com", password: "" },
        { email: "negativetest@gmail.com", password: "      " },
        { email: "negativetest@gmail.com", password: "npassword " },
        { email: "negativetest@gmail.com", password: "npasword" },
        { email: "alex.qa.test@gmail.com", password: "npass" },
    ];

    invalidCredentialsDataSet.forEach((credentials) => {
        it(`should not login using invalid credentials : '${credentials.email}' + '${credentials.password}'`, async () => {
            let response = await auth.login(credentials.email, credentials.password);

            checkStatusCode(response, 401);
            checkResponseTime(response, 3000);
        });
    });
});

describe("Use test data set for registration", () => {
    let invalidRegistrationUserDataSet = [
        { avatar: "string", email: "", userName: "newuser12", password: "password1212" },
        { avatar: "string", email: "invalidemail", userName: "newuser12", password: "password1212" },
        { avatar: "string", email: "newuser19@gmail.com", userName: "", password: "password1212" },
        { avatar: "string", email: "newuser19@gmail.com", userName: "newuser12", password: "" },
        { avatar: "string", email: "newuser19@gmail.com", userName: "newuser12", password: "123" },
        { avatar: "string", email: "newuser19@gmail.com", userName: "n", password: "12345678" },
    ];

    invalidRegistrationUserDataSet.forEach((userData) => {
        it(`should not register using invalid user data: ${JSON.stringify(userData)}`, async () => {
            let response = await users.createUser(userData);

            checkStatusCode(response, 400);
            checkResponseTime(response, 3000);
        });
    });
});
