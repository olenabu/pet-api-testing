import { expect } from "chai";
import { UsersController } from "../lib/controllers/users.controller";
import { AuthController } from "../lib/controllers/auth.controller";
import {
    checkStatusCode,
    checkResponseTime,
    checkUserProperties,
} from "../../helpers/functionsForChecking.helper";
import { generateRandomString } from "../../helpers/functionsForRandomstring.helper";
import chai from "chai";
import chaiHttp from "chai-http";

chai.use(chaiHttp);

const users = new UsersController();
const auth = new AuthController();

chai.use(chaiHttp);

let userId: number;
let createdUser: any;
let userEmail: string;
let userPassword: string;
let authToken: string;
let refreshToken: string;
let updatedUserName: string;
let userInfoFromTokenBeforeUpdate: any;

before(async () => {
    // Create a new user
    const newUser = {
        avatar: "string",
        email: "newuser30@gmail.com",
        userName: "newuser12",
        password: "password1212",
    };

    const response = await users.createUser(newUser);

    checkStatusCode(response, 201);
    checkResponseTime(response, 3000);

    createdUser = response.body.user;
    userId = createdUser.id;
    userEmail = newUser.email;
    userPassword = newUser.password;

    console.log("Created User ID:", userId);

    // Authenticate the user and save the authToken and refreshToken
    const authResponse = await auth.login(userEmail, userPassword);

    checkStatusCode(authResponse, 200);
    checkResponseTime(response, 3000);

    authToken = authResponse.body.token.accessToken.token;
    refreshToken = authResponse.body.token.refreshToken;

    //Get user info from token before updating the user
    const userInfoResponseBeforeUpdate = await users.getCurrentUser(authToken);

    checkStatusCode(userInfoResponseBeforeUpdate, 200);
    checkResponseTime(response, 3000);
    userInfoFromTokenBeforeUpdate = userInfoResponseBeforeUpdate.body;
});

it("User is created", async () => {
    expect(createdUser).to.exist;

    const response = await users.getUserById(userId);

    checkStatusCode(response, 200);
    checkResponseTime(response, 3000);
    expect(response.body.email).to.equal(createdUser.email);
    expect(response.body.userName).to.equal(createdUser.userName);
});

it(`Users list. Should return a status code 200 and non-empty response`, async () => {
    const response = await users.getAllUsers();

    checkStatusCode(response, 200);
    checkResponseTime(response, 3000);
    expect(response).to.not.be.empty;
});

it("Get user info from token before updating user. Should return user info based on token", () => {
    expect(userInfoFromTokenBeforeUpdate).to.exist;
    expect(userInfoFromTokenBeforeUpdate.id).to.equal(userId);
    expect(userInfoFromTokenBeforeUpdate.email).to.equal(userEmail);
    expect(userInfoFromTokenBeforeUpdate.userName).to.equal(createdUser.userName);
});

it("Update User. Should update user information", async () => {
    updatedUserName = generateRandomString(10);

    const updatedUserData = {
        id: userId,
        avatar: createdUser.avatar,
        email: userEmail,
        userName: updatedUserName,
    };

    const response = await users.updateUser(updatedUserData, authToken);

    checkStatusCode(response, 204);
    checkResponseTime(response, 3000);

    const updatedUserInfoResponse = await users.getUserById(userId);

    checkStatusCode(updatedUserInfoResponse, 200);
    checkResponseTime(response, 3000);
    expect(updatedUserInfoResponse.body.id).to.equal(userId);
    expect(updatedUserInfoResponse.body.email).to.equal(userEmail);
    expect(updatedUserInfoResponse.body.userName).to.equal(updatedUserName);
});

it("Get user info from token after updating user. Should return updated user info based on token", async () => {
    const userInfoResponseAfterUpdate = await users.getCurrentUser(authToken);

    checkStatusCode(userInfoResponseAfterUpdate, 200);
    checkResponseTime(userInfoResponseAfterUpdate, 3000);

    const userInfoFromTokenAfterUpdate = userInfoResponseAfterUpdate.body;

    expect(userInfoFromTokenAfterUpdate).to.exist;
    expect(userInfoFromTokenAfterUpdate.id).to.equal(userId);
    expect(userInfoFromTokenAfterUpdate.email).to.equal(userEmail);
    expect(userInfoFromTokenAfterUpdate.userName).to.equal(updatedUserName);
});

describe("Get User by ID", () => {
    it("Get User by ID. Should return user info by ID and match with saved info", async () => {
        const response = await users.getUserById(userId);

        checkStatusCode(response, 200);
        checkResponseTime(response, 3000);
        checkUserProperties(response, userId, userEmail, updatedUserName);
    });
});

after(async () => {
    // Delete the user after all the tests are completed
    const deleteUserResponse = await users.deleteUser(userId, authToken);

    checkStatusCode(deleteUserResponse, 204);
    checkResponseTime(deleteUserResponse, 3000);
    console.log("User deleted after tests:", userId);
});
